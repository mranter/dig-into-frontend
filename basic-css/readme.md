# CSS 介绍

## 什么是 CSS

- CSS 指层叠样式表 (Cascading Style Sheets)
- 样式定义如何显示控制 HTML 元素,从而实现美化 HTML 网页。

## CSS 语法

> 选择器{属性:值;属性:值}

![选择器](./img/selector.gif)

> 范例

```css
p {
  color: red;
  text-align: center;
}
```

## CSS 注释

> 注释方法

```css
/* ... */
```

## HTML 中应用 CSS 样式的方法

1. 行内样式

   > 注释

   所谓行内样式就是在 HTML 标签中使用 style 属性来设置 CSS 样式

   > 示例

   ```html
   <p style="color:blue;">在HTML中如何使用css样式</p>
   ```

2. 内嵌样式

   > 注释

   所谓内嵌样式就是在 HTML 中使用 `<style>` 标签来设置 CSS 样式

   > 示例

   ```html
   <style>
     p {
       color: blue;
     }
   </style>
   <p>在HTML中如何使用css样式</p>
   ```

3. 外链样式

   > 注释

   所谓外链样式就是在 HTML 的 `head` 标签中使用 `<link>` 标签指定一个 CSS 样式文件

   > 示例

   ```html
   <link href="style.css" type="text/css" rel="stylesheet" />
   <p>在HTML中如何使用css样式</p>
   ```

   在 CSS 文件中

   ```css
   ... p {
     color: blue;
   }
   ```

## 选择器

前面的课程中我们已经讲过如何给 HTML 指定样式了。这里我们主要来介绍选择器。

在 HTML 中选择器是非常重要的一个知识点，由于灵活多变，种类繁多，因此显得有些难。下面我们一一讲解

### 标签选择器

标签选择器是直接使用 HTML 中的标签作为选择器。

```css
p {
  ...;
}

h1 {
  ...;
}
```

### 类选择器

类选择器是使用最广泛的一种选择器，类即 `class` 是 HTML 标签的一个全局属性「每一个标签都具有的属性」，展现形式如下：

```html
<style>
  .text {
    font-size: 16px;
    color: blue;
  }
</style>
<p class="text">loren inspect...</p>
```

### ID 选择器

ID 选择器是广泛使用的一种选择器，`id` 是 HTML 标签的一个全局属性，**注意：** ID 选择器在网页中只能使用一次，因此不常用来指定样式。展现形式如下：

```html
<style>
  #text {
    font-size: 16px;
    color: blue;
  }
</style>
<p id="text">loren inspect...</p>
```

## 盒子模型

开始讲盒子模型之前，我们来回顾一下盒子（block），你应该还有印象，就是前几周讲过的内容中的块级元素。我们说块级元素是占据浏览器整行的宽度。那我们是否可以手动来限制一个块级元素的大小呢？答案当然是可以的。我们来看看具体要如何执行。

### 块级元素

首先我们看看块级元素在浏览器中是如何展示的，我们打开浏览器，进入调试面板「通常按 F12」。看到下面图所示：
![block](./img/block.png)

仔细看上图，看途中红色框中的内容，默认情况下块级元素的 `display` 属性是 `block`，如果我们把这个改成 `inline` 会法神什么呢？动手尝试一下。

到这里我们终于明白块级元素和行内元素的本质所在了。那么块级元素相对于行内元素有哪些好处呢？我们继续往下看。

### 块级元素的尺寸

既然默认情况下块级元素是占据整行的宽度，假设我们现在的需要一个正方形用来存放我们的内容，那么有什么办法呢？

```css
div {
  width: 320px;
  height: 320px;
}
```

上面的代码我们将该块级元素的大小设置为宽高都是 320 像素的正方形，问题：之后的内容会顶上来吗？

### 盒子模型图示

![standard box model](img/standard-box-model.png)

上面我们已经解决了盒子模型的尺寸问题，现在我们来聊一下盒子模型还有那些特点，请看下图：
![blocks model](./img/box-model.png)

我们给代码加上以下属性：

```css
div {
  ... padding: 20px;
  margin: 40px;
  border: 3px solid #999;
}
```

### 悬浮

前面这个问题，我想大家测试过后一定发现后面的内容是无法顶上来的，为什么？原因就在于，虽然你限制的盒子的尺寸，但是，盒子的特征依然存在。

那怎么办呢？方法就是让这个盒子浮起来。方法是使用 CSS 中的 `float` 方法。具体用法如下：

```css
div {
  ... float: left;
}
```

### 解决悬浮后遗症

在我们演示的过程中发现一个问题就是，使用悬浮之后，破环了网页的样式，原因就在于，使用悬浮之后，该盒子不在占据空间，要解决这个问题有两种方式。

#### 方式一：clear

```css
.fix {
  clear: both;
}
```

#### 方式二：display

```css
.group::after {
  content: "";
  display: table;
  clear: both;
}
```

> 本节作业，利用盒子模型布局网页。
