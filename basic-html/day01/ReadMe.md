# 什么是 HTML

HTML 全称 Hyper Text Markup Language 意思是「超文本标记语言」。这是相对于文本文件来说的，因此为了明白什么是「超文本标记语言」，我们先看看什么是文本。

请依次执行 `开始 => 运行 => notepad` 或者执行快捷键 `win + R` 或者手动打开记事本应用。

## 对比

### 记事本

- 只可以记录文字
- 只可以整体应用样式
- ...

### HTML

- 可以记录图片
- 可以记录链接
- 可以单一调整样式
- ...

## 如何编写 HTML 文件

HTML 文件的后缀是 `.html`，在计算机中后缀通常用来标记文件类型。我们说 `.html` 是 HTML 文件 `.txt` 是文本文件，`.mp3` 是音频文件，`.mp4` 是视频文件等等，都是根据后缀来判断文件。

### HTML 结构

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <!-- 这里用来编写网页的正文 -->
  </body>
</html>
```
