# 表单

这节课我们将开始学习 HTML 中的表单，表单一般用来向服务器提交用户数据，比如常见的注册表单，联系表单，登录表单等等。

- [表单](#%e8%a1%a8%e5%8d%95)
  - [`<form>`](#form)
    - [Sample Contact Form](#sample-contact-form)
    - [input 标签](#input-%e6%a0%87%e7%ad%be)
      - [input 常用类型](#input-%e5%b8%b8%e7%94%a8%e7%b1%bb%e5%9e%8b)
      - [下拉选择框](#%e4%b8%8b%e6%8b%89%e9%80%89%e6%8b%a9%e6%a1%86)
      - [单选按钮](#%e5%8d%95%e9%80%89%e6%8c%89%e9%92%ae)
      - [复选框](#%e5%a4%8d%e9%80%89%e6%a1%86)
      - [按钮](#%e6%8c%89%e9%92%ae)
  - [其他表单控件](#%e5%85%b6%e4%bb%96%e8%a1%a8%e5%8d%95%e6%8e%a7%e4%bb%b6)

![form](./imgs/form-sketch-low.jpg)

## `<form>`

在 HTML 中，所有表单元素都存放在 \<form>这个标签中。写法如下:

```html
<form action="/handling-form-page" method="post">
  ...
</form>
```

这里有两个属性需要了解：

> `action`:属性定义了在提交表单时,应该把所收集的数据提交给谁(/那个模块)(URL)去处理。  
> `methor`:属性定义了发送数据的 HTTP 方法(它可以是“get”或“post”).

### Sample Contact Form

```html
<form action="/handling-form-page" method="post">
  <label for="name">姓名:</label>
  <input type="text" id="name" />

  <label for="mail">电子邮箱:</label>
  <input type="email" id="mail" />

  <label for="msg">信息:</label>
  <textarea id="msg"></textarea>
</form>
```

通过观察上面的表单，我们可以发现，每一个表单字段都有一个 label 标签，然后一个控件标签（input 和 textarea），那么这些标签都是什么意思呢？

> `label`：这个标签用来描述相对应的输入标签，让标签文本与输入框在视觉上产生关联与交互，当我们点击标签的时候，会发现相应的输入标签处于激活状态。这就是`label`标签中`for`属性的作用。注意这个属性的值必须与对于输入标签中的`id`值相对应。

> `textarea`：这个标签是多行文本框，这个标签有两个属性需要了解一下。
>
> - `rows`:元素的输入文本的行数（显示的高度）。
> - `cols`:文本域的可视宽度。必须为正数，默认为 20 (HTML5)。

### input 标签

`<input>` 是最基本的表单小部件。 这是一种非常方便的方式，可以让用户输入任何类型的数据。这主要通过设定`<input>` 的`type`属性来实现,下面通过`type`值不同来讲解。

<!-- omit in toc -->

#### input 常用类型

> `type="text"`：单行文本框，这是`input`标签的默认值。  
> `type="email"`：电子邮件输入框，将校验用户输入的是否是合法的电子邮箱。  
> `type="password"`：密码输入框，将隐藏用户输入的信息。  
> `type="url"`：URL 输入框，将验证用户输入的是否是合法的链接。  
> `type="search"`：搜索输入框，与文本框的主要区别在于浏览器的样式。  
> `type="tel"`：电话号码输入框。  
> ...

<!-- omit in toc -->

#### 下拉选择框

```html
<p>
  <label for="groups">Select box with option groups:</label>
  <select id="groups" name="groups">
    <optgroup label="fruits">
      <option>Banana</option>
      <option selected>Cherry</option>
      <option>Lemon</option>
    </optgroup>
    <optgroup label="vegetables">
      <option>Carrot</option>
      <option>Eggplant</option>
      <option>Potato</option>
    </optgroup>
  </select>
</p>
```

一个选择框是用`<select>`元素创建的，其中可以有一个或多个`<option>`元素作为子元素，每个元素都指定了其中一个可能的值。如果需要，可以使用`selected`属性在所需的`<option>`元素上设置选择框的默认值,一旦设置，那么该选项将成为默认值。`<option>`元素也可以嵌套在`<optgroup>`元素中，以创建分组的选项。

一个更好的方式是在`<option>`元素中设置了`value`属性，这样当我们提交表单的时候，`value`的值将作为该选项的值发送到服务器，如果没设置，将使用该元素中的内容作为值发送。

<!-- omit in toc -->

#### 单选按钮

当`<input>`标签中`type="radio"`时，那么将创建一个单选选框，请看下面这个例子。

```html
<fieldset>
  <legend>你最喜欢的食物是什么?</legend>
  <ul>
    <li>
      <label for="soup">汤</label>
      <input type="radio" checked id="soup" name="meal" value="汤" />
    </li>
    <li>
      <label for="curry">咖喱饭</label>
      <input type="radio" id="curry" name="meal" value="咖喱饭" />
    </li>
    <li>
      <label for="pizza">披萨</label>
      <input type="radio" id="pizza" name="meal" value="披萨" />
    </li>
  </ul>
</fieldset>
```

如果要为一个选项设置多个单选值，那么我们需要将这所有的单选框中的`name`值设置为相同的。那么在这一组的单选框中，如果其中一个被选中，那么其余的选项将自动设为未选中。

<!-- omit in toc -->

#### 复选框

当`<input>`标签中`type="checkbox"`时，那么将创建一个复选框，请看下面这个例子。

```html
<fieldset>
  <legend>请选择你喜欢的食物。</legend>
  <ul>
    <li>
      <label for="soup">汤</label>
      <input type="checkbox" checked id="soup" name="meal" value="汤" />
    </li>
    <li>
      <label for="curry">咖喱饭</label>
      <input type="checkbox" id="curry" name="meal" value="咖喱饭" />
    </li>
    <li>
      <label for="pizza">披萨</label>
      <input type="checkbox" checked id="pizza" name="meal" value="披萨" />
    </li>
    <li>
      <label for="braisedPork">红烧肉</label>
      <input type="checkbox" id="braisedPork" name="meal" value="红烧肉" />
    </li>
  </ul>
</fieldset>
```

如果要为一个选项可以有多个选项，只要将`type="checkbox"`，然后同组的选项可以选择多个值。

<!-- omit in toc -->

#### 按钮

按钮用来将整个表单发送到服务器上。你有两种方式，一种是使用`<button>`元素,另一种是`<input>`元素。这两者之间还是有一些区别的。请看下面的例子：

```html
<!-- submit -->
<button type="submit">这是一个 <br /><strong>提交按钮</strong></button>

<input type="submit" value="这是一个提交按钮" />

<!-- reset -->
<button type="reset">这是一个 <br /><strong>重置按钮</strong></button>

<input type="reset" value="这是一个重置按钮" />

<!-- button -->
<button type="button">这是一个 <br /><strong>未定义按钮</strong></strong></button>

<input type="button" value="这是一个未定义按钮" />
```

通过仔细观察上面的例子，你一定可以发现，这两种虽然可以做同样的事情，但是`<button>`元素可以内嵌 HTML 代码。

## 其他表单控件

当然除了上面这些还有一些控件我们可以去使用，限于篇幅，我们就不展开讲解了，大家可以看下面的例子。

```html
<form>
  <p>
    <label for="age">你的年龄？</label>
    <input type="number" name="age" id="age" min="1" max="10" step="2" />
  </p>
  <p>
    <label for="beans">你一次可以吃多少豆子？</label>
    <input type="range" name="beans" id="beans" min="0" max="500" step="10" />
    <span class="beancount">250</span>
  </p>
  <p>
    <label for="myDate">夏天开始和结束的时间？</label>
    <input type="date" name="myDate" min="2013-06-01" max="2013-08-31" id="myDate" />
  </p>
  <p>
    <label for="meet">我们什么时候可以再见面？</label>
    <input type="datetime-local" name="meet" id="meet" />
  </p>
  <p>
    <label for="month">你最喜欢哪个月？</label>
    <input type="month" name="month" id="month" />
  </p>
  <p>
    <label for="time">设置你 WIFI 的关闭时间。</label>
    <input type="time" name="time" id="time" />
  </p>
  <p>
    <label for="color">你最喜欢什么颜色？</label>
    <input type="color" name="color" id="color" />
  </p>
</form>
```
