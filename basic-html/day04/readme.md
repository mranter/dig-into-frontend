# 块级标签与行内标签

这节课时 HTML 课程的最后一节课，这节课之后我们将开始学习 CSS。这节课的内容对于之后的课程至关重要，请大家一定要耐下性子仔细听。

## 行内标签
默认情况下，行内元素只占用该内容的宽度，不会换行，而块级标签正好相反。

在 HTML 5 中常用的行内标签如下：

i, small
abbr, code, em, strong
a, br, img, map, span, sub, sup
button, input, label, select, textarea


## 块级标签


在 HTML 5 中常用的块级标签如下：

aside: HTML5, 伴随内容。
article: HTML5, 文章内容。
canvas: HTML5, 绘制图形。
section: HTML5,一个页面区段。
header: HTML5,区段头或页头。
div:文档分区。
footer: HTML5, 区段尾或页尾。

hr:水平分割线。

noscript:不支持脚本或禁用脚本时显示的内容。
ol:有序列表。
ul: 无序列表。
h1,h2,h3,h4,h5,h6: 标题级别 1-6.
hgroup: HTML5,标题组。
p: 行。
blockquote: 块引用。
pre: 预格式化文本。

output: HTML5,表单输出。
dd: 定义列表中定义条目描述。
dl:定义列表。
table: 表格。
tfoot: 表脚注。

audio: HTML5, 音频播放。
video: HTML5,视频。
figcaption: HTML5, 图文信息组标题
figure: HTML5:图文信息组 (参照 figcaption)。
fieldset: 表单元素分组。
address: 联系方式信息。
form: 表单。